import React from 'react';
import { useAsync } from 'react-async';
import axios from 'axios';

// material
import { Card, Stack, Container, Typography, Avatar } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';

// components
import Page from '../components/Page';
import Scrollbar from '../components/Scrollbar';

// eth, gala price.
// https://api.binance.com/api/v3/ticker/price
// ----------------------------------------------------------------------

const DATA_HEAD = [
  { field: 'assetId', flex: 1 },
  {
    field: 'name',
    flex: 1,
    renderCell: (params) => (
      <Stack direction="row" alignItems="center" spacing={2}>
        <Avatar alt={params.row.name} src={params.row.imageUrl} />
        <Typography variant="subtitle2" noWrap>
          {params.row.name}
        </Typography>
      </Stack>
    )
  },
  { field: 'voxId', flex: 1 },
  { field: 'imageUrl', flex: 1, hide: true },
  { field: 'updatedAt', flex: 1, type: 'dateTime' }
];

// ----------------------------------------------------------------------

const loadCollectVox = async ({ page, pageSize, sortModel }) => {
  const VISIBLE_COLUMNS = ['assetId', 'name', 'voxId', 'imageUrl', 'updatedAt'];

  const splitSortModel = (sorts) => {
    const sortKeys = [];
    const sortValues = [];
    if (sorts) {
      sorts.forEach((s) => {
        sortKeys.push(s.field);
        sortValues.push(s.sort);
      });
    }
    return [sortKeys, sortValues];
  };

  const [sortKeys, sortValues] = splitSortModel(sortModel);

  const cvtArrayToString = (arr) => {
    if (arr.length === 0) return '[]';
    return `["${arr.join('", "')}"]`;
  };

  const queryBody = {
    query: `{ collectVoxes(page:${page}, pageSize:${pageSize}, sortKeys:${cvtArrayToString(sortKeys)}, sortValues:${cvtArrayToString(
      sortValues
    )}) { ${VISIBLE_COLUMNS.join(' ')} } }`
  };
  return axios
    .post(`http://localhost:8000/dashboard/`, queryBody)
    .then((res) => res.data.data.collectVoxes.map((v) => ({ ...v, id: v.assetId })));
};

export default function CollectVox() {
  const [pageInfo, setPageInfo] = React.useState({
    page: 0,
    pageSize: 50,
    sortModel: [{ field: 'voxId', sort: 'asc' }]
  });

  const {
    data: collectVoxes,
    error,
    isLoading
  } = useAsync({
    promiseFn: loadCollectVox,
    page: pageInfo.page,
    pageSize: pageInfo.pageSize,
    sortModel: pageInfo.sortModel,
    watch: pageInfo
  });

  if (error) return <div>Error ... `${error.message}`</div>;

  return (
    <Page title="CollectVox | NFTCalculator">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            CollectVOX
          </Typography>
        </Stack>

        <Card>
          {/* <UserListToolbar numSelected={selected.length} filterName={filterName} onFilterName={handleFilterByName} /> */}
          <Scrollbar>
            <div style={{ height: '80vh', width: '100%' }}>
              <DataGrid
                columns={DATA_HEAD}
                pagination
                rowCount={8888}
                page={pageInfo.page}
                pageSize={pageInfo.pageSize}
                rows={collectVoxes != null ? collectVoxes : []}
                loading={isLoading}
                paginationMode="server"
                onPageChange={(page) => setPageInfo((prev) => ({ ...prev, page }))}
                onPageSizeChange={(pageSize) => setPageInfo((prev) => ({ ...prev, pageSize }))}
                sortingMode="server"
                sortModel={pageInfo.sortModel}
                onSortModelChange={(sortModel) => setPageInfo((prev) => ({ ...prev, sortModel }))}
              />
            </div>
          </Scrollbar>
        </Card>
      </Container>
    </Page>
  );
}
